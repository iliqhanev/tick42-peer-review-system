![alt text](https://symphony.com/images/partners/tick42.png "Iliq & Nikolai Team Project")

# Tick 42 Peer Review System

Following these instructions you will be able to run this project on your local machine.

## Prerequisites

The technologies below must be installed in order to run this application:

#### Front End:

* Typescript
* Angular 7+
* Angular CLI

#### Back End:

* MySQL/MariaDB client
* NodeJS
* NPM
* NestJS
* Type ORM

## Installation

To run the project:

#### Back End:

* Create new DB schema - testdb - root // root
* Go to tick42-peer-review-system/api

```bash
npm install
npm run typeorm -- migration:generate -n initial
npm run typeorm -- migration:run
npm run seed
npm run start:dev
```

#### Front End:

Go to tick42-peer-review-system/client

```bash
npm install
ng s
```

## Authors

| #   | First Name | Last Name |
| :-: |:----------:| :--------:|
| 1.  | Iliq       | Hanev     |
| 2.  | Nikolai    | Matev     |


## License
[MIT](https://choosealicense.com/licenses/mit/)