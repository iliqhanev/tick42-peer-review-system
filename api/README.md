# Integrating TypeORM with Nest.js

## Setting up .env file and the config service

 - First we need to create the `.env` file in our app root (in the same level as the `package.json` file):

```
PORT=3000
JWT_SECRET=VerySecr3t!
DB_TYPE=mysql
DB_HOST=localhost
DB_PORT=3306
DB_USERNAME=root
DB_PASSWORD=root
DB_DATABASE_NAME=testdb
```

Please remember you need to update your `.gitignore` file - you should never upload the `.env` file to the repository, it should be kept local all the time:

```
.idea/
.vscode/
node_modules/
build/
tmp/
temp/
.env
```

### We need to create our **config** module and service:

 - config.service.ts:

```
import * as Joi from 'joi';
import * as fs from 'fs';
import * as dotenv from 'dotenv';
import { Injectable } from '@nestjs/common';
import { DatabaseType } from 'typeorm';

export interface EnvConfig {
  [key: string]: string;
}

@Injectable()
export class ConfigService {
  private readonly envConfig: EnvConfig;

  constructor(filePath: string = null) {
    let config;
    if (filePath) {
      config = dotenv.parse(fs.readFileSync(filePath));
    } else {
      config = dotenv.config().parsed;
    }
    this.envConfig = this.validateInput(config);
  }

  private validateInput(envConfig: EnvConfig): EnvConfig {
    const envVarsSchema: Joi.ObjectSchema = Joi.object({
      NODE_ENV: Joi.string()
        .valid(['development', 'production', 'test', 'provision'])
        .default('development'),
      PORT: Joi.number().default(3000),
      JWT_SECRET: Joi.string().required(),
      JWT_EXPIRE: Joi.number().default(3600 * 24 * 7),
      DB_TYPE: Joi.string().default('mysql'),
      DB_HOST: Joi.string().default('localhost'),
      DB_PORT: Joi.number().default(3306),
      DB_USERNAME: Joi.string().default('root'),
      DB_PASSWORD: Joi.string().default('root'),
      DB_DATABASE_NAME: Joi.string().required(),
    });

    const { error, value: validatedEnvConfig } = Joi.validate(
      envConfig,
      envVarsSchema,
    );
    if (error) {
      throw new Error(`Config validation error: ${error.message}`);
    }
    return validatedEnvConfig;
  }

  public get port(): number {
    return +this.envConfig.PORT;
  }

  public get jwtSecret(): string {
    return this.envConfig.JWT_SECRET;
  }

  public get jwtExpireTime(): number {
    return +this.envConfig.JWT_EXPIRE;
  }

  public get dbHost(): string {
    return this.envConfig.DB_HOST;
  }

  public get dbPort(): number {
    return +this.envConfig.DB_PORT;
  }

  public get dbUsername(): string {
    return this.envConfig.DB_USERNAME;
  }

  public get dbPassword(): string {
    return this.envConfig.DB_PASSWORD;
  }

  public get dbName(): string {
    return this.envConfig.DB_DATABASE_NAME;
  }

  public get dbType(): DatabaseType {
    return this.envConfig.DB_TYPE as DatabaseType;
  }
}
```

  - config.module.ts

```
import { Module } from '@nestjs/common';
import { ConfigService } from './config.service';

@Module({
  providers: [{
    provide: ConfigService,
    useValue: new ConfigService(),
  }],
  exports: [ConfigService],
})
export class ConfigModule { }
```

## Install all the depencies

`npm install joi dotenv typeorm @nestjs/typeorm mysql`

## Setup TypeORM

 - Initialize TypeORM for the project

`typeorm init`

 - Update `ormconfig.json` to match our **local** database setup and update paths to entities and migrations

```
{
   "type": "mysql",
   "host": "localhost",
   "port": 3306,
   "username": "root",
   "password": "root",
   "database": "testdb",
   "synchronize": false,
   "logging": false,
   "entities": [
      "src/data/entities/**/*.ts"
   ],
   "migrations": [
      "src/data/migration/**/*.ts"
   ],
   "cli": {
      "entitiesDir": "src/data/entities",
      "migrationsDir": "src/data/migration"
   }
}
```

 - Update project folders: at this point there are two entity folders `src/entity` and `src/data/entities`, we're going to work with the latter. We can delete `src/entity`. Also, we should move `migration` into `src/data`.

 - Update the compilation target in `tscongif.json`, otherwise we will receive an error with our `jwt.strategy` service:

`"target": "es2017"`
 
 - Update the user entity:

```
import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, VersionColumn } from 'typeorm';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('nvarchar')
  name: string;

  @Column('nvarchar')
  password: string;

  @Column({unique: true})
  email: string;

  @CreateDateColumn()
  createdOn: Date;

  @UpdateDateColumn()
  updatedOn: Date;

  @VersionColumn()
  version: number;
}
```

 - Update `package.json` so we can run `typeorm` cli:

```
   "scripts": {
      ...
      "typeorm": "ts-node ./node_modules/typeorm/cli.js"
   },
```

 - Generate and execute a migration:

`npm run typeorm -- migration:generate -n initial`

`npm run typeorm -- migration:run`

 - Import the **config** module in `app.module.ts` and initialize `TypeOrmModule` at root level:

```
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { CoreModule } from './core/core.module';
import { AuthModule } from './auth/auth.module';
import { ConfigModule } from './config/config.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigService } from './config/config.service';

// type, host, port, username, password, database, entities

@Module({
  imports: [
    CoreModule,
    AuthModule,
    ConfigModule,
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        type: configService.dbType as any,
        host: configService.dbHost,
        port: configService.dbPort,
        username: configService.dbUsername,
        password: configService.dbPassword,
        database: configService.dbName,
        entities: ['./src/data/entities/*.ts'],
      }),
    }),
  ],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}
```

TypeORM is now setup for our app. What we need to do is update all our controllers - they need to be `async`, the services working with the database should implement `async` methods. We should inject **TypeOrmModule** everywhere we need to access the database.

## Update the Core module and the Users service

 - We need to add **TypeOrmModule** in the **Core** module and declare all the entities we will be accessing through their repositories:

```
import { Module } from '@nestjs/common';
import { UsersService } from './services/users.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../data/entities/user';

@Module({
  imports: [
    TypeOrmModule.forFeature([User]),
  ],
  providers: [UsersService],
  exports: [UsersService],
})
export class CoreModule {}
```

 - And then we need to update our Users service:

```
import { Injectable, Post } from '@nestjs/common';
import { User } from '../../data/entities/user';
import { UserLoginDTO } from '../../models/user/user-login-dto';
import { JwtPayload } from '../interfaces/jwt-payload';
import { UserRegisterDTO } from '../../models/user/user-register-dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
  ) {
  }

  async signIn(user: UserLoginDTO): Promise<User | undefined> {
    return await this.userRepository.findOne({
      where: {
        ...user,
      },
    });
  }

  async register(user: UserRegisterDTO): Promise<User | undefined> {
    const savedUser = await this.userRepository.save({...user});

    return savedUser;
  }

  async validate(payload: JwtPayload): Promise<User | undefined> {
    return await this.userRepository.findOne({
      where: {
        ...payload,
      },
    });
  }

}
```

## Update the Auth module, service and controller

 - First we need to update our `jwt.strategy.ts`, we need to make sure everything is `async`

```
import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { AuthService } from '../auth.service';
import { config } from '../../common/config';
import { JwtPayload } from '../../core/interfaces/jwt-payload';
import { User } from '../../data/entities/user';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly authService: AuthService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      // Change to the actual config service, don't use the hardcoded config object
      secretOrKey: config.jwtSecret,
    });
  }

  async validate(payload: JwtPayload): Promise<User> {
    const user = await this.authService.validateUser({email: payload.email});
    if (!user) {
      throw new Error(`Not authorized!`);
    }

    return user;
  }
}
```

 - Next we need to update the Auth service with `async` methods:

```
import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../core/services/users.service';
import { UserLoginDTO } from '../models/user/user-login-dto';
import { JwtPayload } from '../core/interfaces/jwt-payload';
import { User } from '../data/entities/user';

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly usersService: UsersService,
  ) {}

  async signIn(user: UserLoginDTO): Promise<string> {
    const userFound = await this.usersService.signIn(user);
    if (userFound) {
      return await this.jwtService.sign({email: userFound.email});
    }

    return null;
  }

  async validateUser(payload: JwtPayload): Promise<User | undefined> {
    return await this.usersService.validate(payload);
  }
}
```

 - And finally we update the Auth controller with `async` methods:

```
import { Controller, Post, Body, ValidationPipe, BadRequestException } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UsersService } from '../core/services/users.service';
import { UserLoginDTO } from '../models/user/user-login-dto';
import { UserRegisterDTO } from '../models/user/user-register-dto';
import { User } from '../data/entities/user';

@Controller('')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
  ) {}

  @Post('login')
  async login(@Body(new ValidationPipe({
    transform: true,
    whitelist: true,
  })) user: UserLoginDTO): Promise<{token: string}> {
    const token = await this.authService.signIn(user);
    if (!token) {
      throw new BadRequestException(`Wrong credentials!`);
    }

    return { token };
  }

  @Post('register')
  async register(@Body(new ValidationPipe({
    transform: true,
    whitelist: true,
  })) user: UserRegisterDTO): Promise<User> {
    return await this.usersService.register(user);
  }
}
```

Now we should have anything needed to begin extending our app with more entities and functionality.