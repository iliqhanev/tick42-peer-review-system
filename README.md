![alt text](https://symphony.com/images/partners/tick42.png "Iliq & Nikolai Team Project")

# Tick 42 Peer Review System

Following these instructions you will be able to run this project on your local machine.

## Prerequisites

The technologies below must be installed in order to run this application:

#### Front End:

* Typescript
* Angular 7+
* Angular CLI

#### Back End:

* MySQL/MariaDB client
* NodeJS
* NPM
* NestJS
* Type ORM

## Installation

To run the project:

#### Back End:

* Create new DB schema - testdb - root // root
* Go to tick42-peer-review-system/api

```bash
npm install
npm run typeorm -- migration:generate -n initial
npm run typeorm -- migration:run
npm run seed
npm run start:dev
```
#### FrontEnd  available on : https://testproject-3bdb3.firebaseapp.com/home
#### Backend Deployed on : https://dashboard.heroku.com/apps/tick-gitparty

 ((You may experience unauthorized access because free tier of heroku doesn't support https ,so let us know who to send an invitation to at - iliqhanev@gmail.com))
 Or you can even download the project and run it on your own local machine))

 
#### Front End:
Go to tick42-peer-review-system/client

```bash
npm install
ng s
```

## Home Page

![alt text](https://i.imgur.com/MR501dj.png "Iliq & Nikolai Team Project")

## Authors

| #   | First Name | Last Name |
| :-: |:----------:| :--------:|
| 1.  | Iliq       | Hanev     |
| 2.  | Nikolai    | Matev     |


## License
[MIT](https://choosealicense.com/licenses/mit/)